﻿# Mercados El Baratón

Esta es una aplicación donde mercados El Baratón ofrece una variedad de productos que se clasifican a través de una serie de categorías.

A través de esta aplicación los podrás escoger los productos que deseaa adquirir y agregarlos a la canasta para su compra.

Con mercados El Baratón podrás adquirir productos de forma rápida y dicertida a través de sus diferentes filtros, y si te parece poco, puedes utilizarla también en tu celular y comprar en donde quiera que estes.

![Paper written in LaTeX](https://s3-sa-east-1.amazonaws.com/media-dev.kronoapp.co/1q2w3e4r.png)

## Quick Links:

* [Demo](http://mercadoselbaraton.s3-website-us-east-1.amazonaws.com/)
* [Installing](#installing)
* [Building](#building)

## <a name="installing"></a> Installing

Lo primero es asegurarse de tener las versiones correctas de **node** y **npm**

```bash
# node -v
v6.11.0

# npm -v
3.10.10
```

Puedes instalarlas facilmente siguiendo estos comandos:

```bash
# Usando curl
curl -o- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash

# usando wget
wget -qO- https://raw.githubusercontent.com/creationix/nvm/v0.33.2/install.sh | bash
```

Luego

```bash
export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && . "$NVM_DIR/nvm.sh" # This loads nvm
## <a name="building"></a> Building
```

Por último

```bash
nvm install v6.11.0
```

### Clonar repo

```bash
git clone https://oskcolorado@bitbucket.org/oskcolorado/elbaraton.git
```

Instalamos o actualizamos las **npm** dependencias:

```bash
npm install
```

Ahora estamos listos para correr el proyecto:

```bash
npm start
```

## <a name="building"></a> Building

Para construir la versión de producción utilizar:

```bash
ng build -prod
```

***