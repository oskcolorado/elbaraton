import { BrowserAnimationsModule } from '@angular/platform-browser/animations'; // this is needed!
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { RouterModule } from '@angular/router';
//import { AppRoutingModule } from './app.routing';
import { ExamplesModule } from './examples/examples.module';
import { HomeModule } from './home/home.module';
import { HomeComponent } from './home/home.component';
import { JWBootstrapSwitchModule } from 'jw-bootstrap-switch-ng2';
import { NouisliderModule } from 'ng2-nouislider';
import { ProductsComponent } from './home/products/products.component';
import { AppComponent } from './app.component';
import { NavbarComponent } from './shared/navbar/navbar.component';
import { routing } from './app.routing';
import { HttpModule } from '@angular/http';
import { LocalStorageModule } from 'angular-2-local-storage';
import { BasketComponent } from './home/basket/basket.component';
import { AddProductComponent } from './shared/add-product/add-product.component';
import { AddProductModule } from './shared/add-product/add-product.module';
import { 
    ProductsFilterById,
    SortPipe,
    FilterByQuantity,
    FilterPriceRange,
    FilterByAvalability
} from '../pipes/filter_sublevel';

@NgModule({
    declarations: [
        AppComponent,
        NavbarComponent,
        HomeComponent,
        BasketComponent,
        ProductsComponent,
        AddProductComponent,
        ProductsFilterById,
        SortPipe,
        FilterByQuantity,
        FilterPriceRange,
        FilterByAvalability
    ],
    imports: [
        BrowserAnimationsModule,
        NgbModule.forRoot(),
        FormsModule,
        RouterModule,
        HttpModule,
        //AppRoutingModule,
        ExamplesModule,
        // El appId es un identificador único en la página
        BrowserModule.withServerTransition({appId: 'elbaraton'}),
        routing,
        HomeModule,
        LocalStorageModule.withConfig({
            prefix: 'elbaraton',
            storageType: 'localStorage'
        }),
        AddProductModule,
        JWBootstrapSwitchModule,
        NouisliderModule
    ],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule { }
