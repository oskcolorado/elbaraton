import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';
import { ModuleWithProviders } from '@angular/core';
import { HomeComponent } from './home/home.component';

const routes: Routes =[
	{ path: 'home',                                       component: HomeComponent },
	{ path: 'home/category/:category_id',                 component: HomeComponent },
    { path: '', redirectTo: 'home', pathMatch: 'full' }
];

export const routing: ModuleWithProviders = RouterModule.forRoot(routes, { useHash: true });

export class AppRoutingModule { }
