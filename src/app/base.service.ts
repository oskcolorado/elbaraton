import { Injectable } from '@angular/core';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import { Router } from '@angular/router';
// Import RxJs required methods
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';

@Injectable()
export class BaseService {

    data: any;
    path: string = 'assets/data/';

    constructor(private http: Http) {
        this.data = null;
    }

    getBase(route, options=null){
        return this.http.get(this.path+route)
            .map((res: any) => {
                return res.json();
            }
        )
    }

    save(endpoint, payload, options=null) {
        return this.http.post(this.path + endpoint, payload)
            .map((res: any) => {
                return res.json();
            }
        )
    };

}
