import { Component, OnInit, Input } from '@angular/core';
import { ProductsService } from '../products.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-basket',
  templateUrl: './basket.component.html',
  styleUrls: ['./basket.component.scss']
})
export class BasketComponent implements OnInit {

    tmp: any = [];
    @Input() basket: any;
    constructor(private _products_service: ProductsService,
                private localStorage: LocalStorageService) { }

    ngOnInit() {

    };

    removeItem(index) {
        this.basket.splice(index, 1);
        this.localStorage.set('basket', this.basket);

    };

}
