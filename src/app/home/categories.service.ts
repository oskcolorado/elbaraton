import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

@Injectable()
export class CategoriesService extends BaseService {


    get_categories() {
        let observers = new BehaviorSubject(null);
        this.getBase('categories.json')
            .subscribe(datos => {
                observers.next(datos.categories);
            }, error => {
                observers.next(error);
            });
        return observers;
    } 

}