import { Component, OnInit, OnDestroy } from '@angular/core';
import { CategoriesService } from './categories.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
    styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
    data : Date = new Date();
    categories: any = [];

    constructor(private _categories_service: CategoriesService,
                private localStorage: LocalStorageService) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('blog');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');

        /* Get categories */
        this._categories_service.get_categories().subscribe(categories => {
            if (categories) {
                this.categories = categories;
                for (var i in categories) {
                    this.categories[i]['category'] = true;
                }
                //console.log(this.categories);
            }
        });
    };

    save_category(sublevel) {
        this.localStorage.set('current_category', sublevel);
    };

    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('blog');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    };

}