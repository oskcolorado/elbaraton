import { Injectable } from '@angular/core';
import { BaseService } from '../base.service';
import { Http, Headers, Response, RequestOptions } from '@angular/http';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map'

@Injectable()
export class ProductsService extends BaseService {


    get_products() {
        let observers = new BehaviorSubject(null);
        this.getBase('products.json')
            .subscribe(datos => {
                observers.next(datos.products);
            }, error => {
                observers.next(error);
            });
        return observers;
    } 

}