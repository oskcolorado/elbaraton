import { Component, OnInit, OnDestroy } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ProductsService } from '../products.service';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit, OnDestroy {

    /* Variables */
    data : Date = new Date();
    category_id: number;
    category: any = {};
    products: any = [];
    text: any = null;
    dataForm: string = null;
    order_by: string = '';
    doubleSlider:any = [0,100000];
    p_range:any = [0,100000];
    p_available: boolean = true;
    page: any = 1;

    constructor(private route: ActivatedRoute,
                private router: Router,
                private _products_service: ProductsService,
                private localStorage: LocalStorageService) { }

    ngOnInit() {
        var body = document.getElementsByTagName('body')[0];
        body.classList.add('blog');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.remove('navbar-transparent');

        /* Get category or sublevel of URL */
        this.route.params.subscribe(params => {
            this.category_id = +params['category_id'];
            /* Get category info */
            this.category = this.get_category();
            /* Reset values */
            this.reset_values();
        });

        /* Get products */
        this.get_products();        
    };

    add_product_to_basket(item) {
        let exist: any = false;
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                basket[i].item_quantity += 1;
                exist = true;
                break;
            }
        }
        if(!exist) {
            item.item_quantity = 1;
            basket.push(item);
        }
        this.localStorage.set('basket', basket);
    };
    
    removeItem_in_basket(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                basket[i].item_quantity -= 1;
                break;
            }
        }
        this.localStorage.set('basket', basket);
    };

    find_product_in_basket(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                return basket[i];
            }
        }
        item.item_quantity = 0;
        return item;
    };

    show_item_quantity(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                return basket[i];
            }
        }
    };

    get_products() {
        this._products_service.get_products().subscribe(products => {
            if (products) {
                this.products = products;
            }
        });
    };

    reset_values() {
        this.text = null;
        this.dataForm = null;
        this.order_by = "";
        this.doubleSlider = [0,100000];
        this.p_range = [0,100000];
        this.p_available = true;
    };

    get_category() {
        return this.localStorage.get('current_category');
    };

    is_last_category() {
        if(!this.category_id || this.category.sublevels) {
            return false;
        } else {
            return true;
        }
    };

    search() {
        this.text = this.dataForm;
    };

    change_order(_by) {
        this.order_by = _by;
    };

    ngOnDestroy(){
        var body = document.getElementsByTagName('body')[0];
        body.classList.remove('blog');
        var navbar = document.getElementsByTagName('nav')[0];
        navbar.classList.add('navbar-transparent');
    }
}
