import { Component, OnInit, Input } from '@angular/core';
import { LocalStorageService } from 'angular-2-local-storage';

@Component({
    selector: 'app-add-product',
    templateUrl: './add-product.component.html',
    styleUrls: ['./add-product.component.scss']
})
export class AddProductComponent implements OnInit {

    @Input() item: any;

    constructor(private localStorage: LocalStorageService) {
        
    }

    ngOnInit() {

    };

    add_product_to_basket(item) {
        let exist: any = false;
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                basket[i].item_quantity += 1;
                exist = true;
                break;
            }
        }
        if(!exist) {
            item.item_quantity = 1;
            basket.push(item);
        }
        this.localStorage.set('basket', basket);
    };
    
    removeItem_in_basket(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                basket[i].item_quantity -= 1;
                break;
            }
        }
        this.localStorage.set('basket', basket);
    };
    
    find_product_in_basket(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                return basket[i];
            }
        }
        item.item_quantity = 0;
        return item;
    };

    show_item_quantity(item) {
        let basket: any = this.localStorage.get('basket');
        for(var i in basket) {
            if (basket[i].id == item.id) {
                return basket[i];
            }
        }
    };
}