import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BasketComponent } from '../../home/basket/basket.component';

@NgModule({
	imports: [
		CommonModule
	],
	declarations: [ ],
	exports: [ ],
	providers: [
        BasketComponent
	]
})
export class NavbarModule { }