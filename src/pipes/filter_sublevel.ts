import { Pipe } from '@angular/core';

@Pipe({name: 'sublevelFilter', pure: false})
export class ProductsFilterById {
	transform(products: any, id, text=null) {
		if (id == 0) {
			return products.filter(product => {
				if(text) {
					if(product.name.indexOf(text) > -1) {
						return product;
					}
				} else {
					return product;
				}
			});
		}
		else {
			return products.filter(product => {
				if(text) {
					if(product.sublevel_id == id) {
						if(product.name.indexOf(text) > -1) {
							return product;
						}
					}
				} else {
					if(product.sublevel_id == id) {
						return product;
					}
				}
			});
		}
	}
}

@Pipe({name: "sortBy", pure: false})
export class SortPipe {
  transform(array: Array<string>, args: string): Array<string> {
  	if(args == 'price') {
  		array.sort((a: any, b: any) => {
  			var a_tmp = Object.assign({}, a);
  			var b_tmp = Object.assign({}, b);
  			a_tmp[args] = parseInt(a[args].substr(1));
  			b_tmp[args] = parseInt(b[args].substr(1));
		    if ( a_tmp[args] < b_tmp[args] ){
		    	return -1;
		    }else if( a_tmp[args] > b_tmp[args] ){
		        return 1;
		    }else{
		    	return 0;	
		    }
	    });
	    return array;
  	} else {
	    array.sort((a: any, b: any) => {
		    if ( a[args] < b[args] ){
		    	return -1;
		    }else if( a[args] > b[args] ){
		        return 1;
		    }else{
		    	return 0;	
		    }
	    });
	    return array;
  	}
  }
}

@Pipe({name: 'quantityFilter', pure: false})
export class FilterByQuantity {
	transform(products: any, value) {
		return products.filter(product => {
			if(product['quantity'] >= value[0] && product['quantity'] <= value[1]) {
				return product;
			}
		});
	}
}

@Pipe({name: 'priceRangeFilter', pure: false})
export class FilterPriceRange {
	transform(products: any, value) {
		return products.filter(product => {
			var p_tmp = Object.assign({}, product);
			p_tmp['price'] = parseInt(p_tmp['price'].substr(1));
			if(p_tmp['price'] >= value[0] && p_tmp['price'] <= value[1]) {
				return product;
			}
		});
	}
}

@Pipe({name: 'availableFilter', pure: false})
export class FilterByAvalability {
	transform(products: any, value) {
		return products.filter(product => {
			if(product['available'] == value) {
				return product;
			}
		});
	}
}